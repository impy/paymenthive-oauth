var passport = require('passport');

module.exports = {
  loadPriority:  1000,
  startPriority: 1000,
  stopPriority:  1000,
  initialize: function(api, next){
    api.auth = {};

    api.auth.oauthCall = oauthCall;

    function oauthCall(strategy, scope) {
      return function(req, res, next) {
        passport.authenticate(strategy)(req, res, next);
      };
    }

    next();
  }
};
