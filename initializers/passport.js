var passport = require('passport');
var path = require('path');
var fs = require('fs');

module.exports = {
  loadPriority:  999,
  startPriority: 999,
  stopPriority:  999,
  initialize: function(api, next){
    api.passport = passport;

    // Load strategies
    var dir = path.normalize(api.config.passport.strategiesPath);
    fs.readdirSync(dir).forEach(function(file) {
      api.passport.use(require(api.config.passport.strategiesPath + '/' + file)(api));
    });

    // Mirror raw request to passport middleware (like for express)
    var passportMiddleware =  {
      name: 'setup-passport',
      global: true,
      preProcessor:function (data, next) {
        api.passport.initialize()(data.connection.rawConnection.req, data.connection.rawConnection.res, function () {
          api.passport.session()(data.connection.rawConnection.req, data.connection.rawConnection.res, function () {
            data.connection.rawConnection.req.logIn = function(user, info, done) { data.connection.rawConnection.req.user = user; done(); };
            next();
          });
        });
      }
    };

    // Protect the endpoints here and authenticate them by tokens
    var authMiddleware =  {
      name: 'setup-auth-middleware',
      global: true,
      preProcessor:function (data, next) {
        if (data.connection.rawConnection.req.isAuthenticated()) {
          return next();
        }

        // Required authorization
        if (!!data.actionTemplate.authorization) {
          return passport.authenticate('bearer', { session: false })(data.connection.rawConnection.req, data.connection.rawConnection.res, next);
        }

        next();
      }
    };

    // Register passportjs middleware
    api.actions.addMiddleware(passportMiddleware);
    api.actions.addMiddleware(authMiddleware);

    next();
  },
  start: function(api, next){
    next();
  },
  stop: function(api, next){
    next();
  }
};


