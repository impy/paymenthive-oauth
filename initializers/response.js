module.exports = {
  loadPriority:  1000,
  startPriority: 1000,
  stopPriority:  1000,
  initialize: function(api, next){
    redirectMiddleware = {
      name: 'redirect-feature',
      global: true,
      preProcessor: function (data, next) {
        data.connection.rawConnection.res.redirect = function(redirectURL, next) {
          data.connection.rawConnection.responseHeaders.push(['Location', redirectURL]);
          data.connection.rawConnection.responseHttpCode = 302;

          next(null, true);
        };

        next();
      }
    };

    api.actions.addMiddleware(redirectMiddleware);

    next();
  }
};
