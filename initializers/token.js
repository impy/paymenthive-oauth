var uuid = require('uuid');

module.exports = {
  loadPriority:  1000,
  startPriority: 1000,
  stopPriority:  1000,
  initialize: function(api, next){
    api.token = {};
    api.token.create = create;
    api.token.revoke = revoke;
    api.token.refresh = refresh;

    next();

    function create(field, id, next) {
      var TokenModel = api.mongoose.models.token;
      var TokenData = TokenModel({
        token: uuid.v4(),
        refreshToken: uuid.v4(),
        type: field,
        expired: new Date(new Date().getTime() + api.config.passport.expires_in * 1000)
      });

      TokenData[field] = id;

      new TokenModel(TokenData).save(function(err) {
        next(err, TokenData);
      });
    }

    function revoke(token, next) {
      var TokenModel = api.mongoose.models.token;
      TokenModel.remove({
        token: TokenModel.hash(token),
      }, next);
    }

    function refresh(token, refreshToken, next) {
      var TokenModel = api.mongoose.models.token;
      console.log({
        token: TokenModel.hash(token || ''),
        refreshToken: TokenModel.hash(refreshToken || ''),
      });
      TokenModel.findOne({
        token: TokenModel.hash(token || ''),
        refreshToken: TokenModel.hash(refreshToken || ''),
      }, function(err, result) {
        if (err) { return next(err); }
        if (!result) { return next(new Error('Wrong access_token or refresh_token')); }

        result.remove(function(err) {
          if (err) { return next(err); }
          create(result.type, result[result.type], next);
        });
      });
    }
  }
};
