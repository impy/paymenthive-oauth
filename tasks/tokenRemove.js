exports.task = {
  name:          'tokenRemove',
  description:   'tokenRemove',
  frequency:     3600000,
  queue:         'default',
  plugins:       [],
  pluginOptions: {},

  run: function(api, params, next){
    var TokenModel = api.mongoose.models.token;

    TokenModel.remove({
      expired: {
        $lt: new Date((new Date).getTime() -  7 * 24 * 60 * 60 * 1000) // One week before
      }
    }, function(err) { next(err); });
  }
};
