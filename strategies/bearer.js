var passport = require('passport');
var BearerStrategy = require('passport-http-bearer');

module.exports = function (api) {
  return new BearerStrategy(
    function(token, done) {
      var TokenModel = api.mongoose.models.token;

      TokenModel.findOne({
        token: TokenModel.hash(token),
        expired: {
          $gte: new Date()
        }
      })
      .populate('employer')
      .populate('candidate')
      .exec(function (err, user) {
        if (err) { return done(err); }
        if (!user) { return done(null, false); }

        return done(null, user.candidate || user.employer, { scope: 'all' });
      });
    }
  );
};
