var passport = require('passport');
var LinkedInStrategy = require('passport-linkedin-token-oauth2').Strategy;

module.exports = function (api) {
  return new LinkedInStrategy({
    clientID: api.config.passport.linkedin.clientID,
    clientSecret: api.config.passport.linkedin.clientSecret
  },
  function (accessToken, refreshToken, profile, done) {
    // Set the provider data and include tokens
    var providerData = profile._json;
    providerData.accessToken = accessToken;
    providerData.refreshToken = refreshToken;

    // Create the user OAuth profile
    var providerUserProfile = {
      firstName: profile.name.givenName,
      lastName: profile.name.familyName,
      displayName: profile.displayName,
      email: profile.emails[0].value,
      username: profile.username,
      profileImageURL: (providerData.pictureUrl) ? providerData.pictureUrl : undefined,
      provider: 'linkedin',
      providerIdentifierField: 'id',
      providerData: providerData
    };

    // Save the user OAuth profile
    api.user.saveUserProfile(providerUserProfile, done);
  });
};
