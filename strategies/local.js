var LocalStrategy = require("passport-local").Strategy;

module.exports = function(api) {
  return new LocalStrategy(
    { usernameField: 'email' },
    function(username, password, next){
      var Employer = api.mongoose.models.employer;

      Employer.findOne({ email: username }, function(err, result) {
        if (err) {
          return next(err);
        }

        if (result && result.authenticate(password)) {
          return next(null, result);
        }

        next(null, false, { message: 'Invalid username or password.'});
      });
    }
  );
};
