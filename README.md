Payment Hive OAuth
------------------

Heroku ready Paymeny Hive OAuth application.


## To install:
(assuming you have [node v4.x](http://nodejs.org/) and NPM installed)

`npm install`

## To Run:
`npm start`


## Configuration

Application follows 12factor.net pattern, so there is a few way to configure it.

**by editing files**

`/config/passport.js` OAuth providers configuration and redirect_uris

`/config/mongoose.js` MongoDB confiuration


**by setting enviroment variables**

Variable | Description
-|-
MONGODB | MongoDB server URI (*mongodb://localhost/test*)
FACEBOOK_ID | Facebook App ID
FACEBOOK_SECRET | Facebook App secret
LINKEDIN_ID | LinkedIn App ID
LINKEDIN_SECRET | LinkedIn App Secret

## OAuth URLs

**Facebook**

* http://yoursitename.com/api/auth/facebook

**LinkedIn**

* http://yoursitename.com/api/auth/linkedin


## Authorization with Facebook & LinkedIn
POST Request:

```
curl --data "access_token=[access_token]" http://yoursitename.com/api/auth/facebook

```

OR

```
curl --data "access_token=[access_token]" http://yoursitename.com/api/auth/linkedin

```

Response:

```
HTTP/1.1 200 OK
Access-Control-Allow-Headers: Content-Type
Access-Control-Allow-Methods: HEAD, GET, POST, PUT, PATCH, DELETE, OPTIONS, TRACE
Access-Control-Allow-Origin: *
Connection: keep-alive
Content-Length: 299
Content-Type: application/json; charset=utf-8
Date: Mon, 22 Feb 2016 07:50:58 GMT
X-Powered-By: PaymentHive API

{
    "token": {
        "_id": "56cabde2c0524173438b66b9",
        "created": "2016-02-22T07:50:58.456Z",
        "expired": "2016-02-22T08:50:58.456Z",
        "refreshToken": "fc546863-56f5-437b-9469-db2c5a2d2420",
        "token": "e3b0738a-fb56-4612-a625-81c1e248a2df",
        "candidate": "56c76e0eee83b9a029c56d57",
        "type": "candidate"
    }
}
```



## Authorization by email and password (local strategy)

POST Request:

```
curl --data "email=value1&password=value2" http://yoursitename.com/api/auth/local

```

Response:

```
HTTP/1.1 200 OK
Access-Control-Allow-Headers: Content-Type
Access-Control-Allow-Methods: HEAD, GET, POST, PUT, PATCH, DELETE, OPTIONS, TRACE
Access-Control-Allow-Origin: *
Connection: keep-alive
Content-Length: 299
Content-Type: application/json; charset=utf-8
Date: Mon, 22 Feb 2016 07:50:58 GMT
X-Powered-By: PaymentHive API

{
    "token": {
        "_id": "56cabde2c0524173438b66b9",
        "created": "2016-02-22T07:50:58.456Z",
        "expired": "2016-02-22T08:50:58.456Z",
        "refreshToken": "fc546863-56f5-437b-9469-db2c5a2d2420",
        "token": "e3b0738a-fb56-4612-a625-81c1e248a2df",
        "employer": "56c76e0eee83b9a029c56d57",
        "type": "employer"
    }
}
```

## Register a user

POST Request:

```
curl --data "email=my@service.com&password=value2" http://yoursitename.com/api/users
```

Response:

```
HTTP/1.1 200 OK
Access-Control-Allow-Headers: Content-Type
Access-Control-Allow-Methods: HEAD, GET, POST, PUT, PATCH, DELETE, OPTIONS, TRACE
Access-Control-Allow-Origin: *
Connection: keep-alive
Content-Length: 2
Content-Type: application/json; charset=utf-8
Date: Mon, 22 Feb 2016 07:50:39 GMT
X-Powered-By: PaymentHive API

{
    "user": {
        "__v": 0,
        "_id": "56cad0c1a7ea0fdf46dec943",
        "email": "tsdeeef@tddedt.rur",
        "displayName": "",
        "provider": "local",
        "ABN": ""
    }
}

```

## Secure actions

For protecting endpoints `authorization: true` must be set in action. Use `/actions/status.js` as example

## Access secure actions

To access secure endpoints HTTP Header `Authorization` must be set with Bearer token which you've got after final redirect.

```
GET /api/status HTTP/1.1
Authorization: Bearer 4932e43c-f331-4219-ae72-d533aea2ca0c
Connection: keep-alive
Host: localhost:8080
```


## OAuth dances diagram

### Authorization
![Sequence diagram](./docs/authrorization.svg)

### Token refresh

![Sequence diagram](./docs/token_refresh.svg)

### User create

![Sequence diagram](./docs/user_create.svg)

### User auhtorization

![Sequence diagram](./docs/user_password_auth.svg)
