exports.action = {
  name:                   'companySignUp',
  description:            'companySignUp',
  blockedConnectionTypes: [],
  outputExample:          {
    email: '',
    _id: ''
  },
  matchExtensionMimeType: false,
  version:                1.0,
  toDocument:             true,
  middleware:             [],

  inputs: {
    email: String,
    password: String
  },

  run: function(api, data, next){
    var employer = new api.mongoose.models.employer({
      email: data.params.email,
      password: data.params.password
    });

    if (employer.validateSync()) {
      return next(new Error(employer.validateSync().toString()));
    }

    employer.save(function(err, result) {
      if (err) {
        return next(err);
      }

      data.response.employer = result.toJSON();
      delete data.response.employer.password;

      next();
    });
  }
};
