var util = require('util');

exports.action = {
  name:                   'companyLogin',
  description:            'companyLogin',
  blockedConnectionTypes: [],
  outputExample:          {
    token: {}
  },
  matchExtensionMimeType: false,
  version:                1.0,
  toDocument:             true,
  middleware:             [],

  inputs: {
    email: String,
    password: String
  },

  run: function(api, data, next){
    var req = util._extend({ body: data.connection.params, query: data.connection.params, fingerprint: data.connection.fingerprint }, data.connection.rawConnection.req);
    var res = data.connection.rawConnection.res;
    var connection = data.connection;


     api.passport.authenticate("local", { session: false },
      function (err, user, info, extra) {
        if (err) {
          connection.error = err;
          return next(null, false);
        }

        if (!user) {
          connection.rawConnection.responseHttpCode = 401;
          return next(new Error('Unauthorized: ' + info.message));
        }

        api.token.create('employer', user._id, function(err, token) {
          if (err) {
            return next(err);
          }

          data.response.token = token;
          next();
        });
      }
    )(req, res, next);
  }
};
