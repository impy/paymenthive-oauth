var util = require('util');

exports.action = {
  name:                   'candidateFacebookLogin',
  description:            'candidateFacebookLogin',
  blockedConnectionTypes: [],
  outputExample:          {},
  matchExtensionMimeType: false,
  version:                1.0,
  toDocument:             true,
  middleware:             [],

  inputs: {
    access_token: String
  },

  run: function(api, data, next){
    var req = util._extend(data.connection.rawConnection.req, { body: data.connection.params, query: data.connection.params, fingerprint: data.connection.fingerprint });
    var res = data.connection.rawConnection.res;
    var connection = data.connection;

    api.auth.oauthCall('facebook-token')(req, res, function(err) {
      if (err) { return next(err); }

      api.token.create('candidate', req.user._id, function(err, token) {
        if (err) { return next(err); }

        data.response.token = token;
        next();
      });
    });
  }
};
