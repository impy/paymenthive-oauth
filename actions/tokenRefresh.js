exports.action = {
  name:                   'tokenRefresh',
  description:            'tokenRefresh',
  blockedConnectionTypes: [],
  outputExample:          {
    token: {}
  },
  matchExtensionMimeType: false,
  version:                1.0,
  toDocument:             true,
  middleware:             [],

  inputs: {
    access_token: String,
    refresh_token: String
  },

  run: function(api, data, next){
    api.token.refresh(
      data.params.access_token,
      data.params.refresh_token,
      function(err, token) {
        if (err) {
          return next(err);
        }

        data.response.token = token;
        next();
      }
    );
  }
};
