var mongoose = require('mongoose');
var validator = require('validator');
var bcrypt = require('bcrypt');

var validateLocalStrategyEmail = function (email) {
  return validator.isEmail(email, { require_tld: false });
};

var EmployerSchema = new mongoose.Schema({
  ABN: {
    type: String,
    trim: true,
    default: '',
  },
  displayName: {
    type: String,
    trim: true
  },
  type: {
    type: String,
    default: 'employer',
  },
  email: {
    type: String,
    unique: true,
    lowercase: true,
    trim: true,
    default: '',
    validate: [validateLocalStrategyEmail, 'Please fill a valid email address']
  },
  password: {
    type: String,
    default: ''
  },
  updated: {
    type: Date
  }
});

/**
 * Hook a pre save method to hash the password
 */
EmployerSchema.pre('save', function (next) {
  if (this.password && this.isModified('password')) {
    this.password = this.hashPassword(this.password);
  }

  next();
});

EmployerSchema.methods.hashPassword = function (password) {
  if (typeof password === 'string' && password.match(/^\$2a\$11\$/i) === null) {
    return bcrypt.hashSync(password, 11);
  } else {
    return password;
  }
};

EmployerSchema.methods.authenticate = function (password) {
  return bcrypt.compareSync(password, this.password);
};

module.exports = mongoose.model('Employer', EmployerSchema);
