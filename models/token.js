var mongoose = require('mongoose');
var crypto = require('crypto');

var TokenSchema = new mongoose.Schema({
  token: {
    type: String,
    required: true,
    default: ''
  },
  refreshToken: {
    type: String,
    required: true,
    default: ''
  },
  type: {
    type: String,
    required: true,
    default: ''
  },
  candidate: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Candidate'
  },
  employer: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Employer'
  },
  expired: {
    type: Date
  },
  created: {
    type: Date,
    default: Date.now
  }
});

TokenSchema.statics.hash = function (token) {
  return crypto.createHash('sha256').update(token).digest('hex');
};

TokenSchema.pre('save', function (next) {
  if (this.token && this.isModified('token')) {
    this.token = TokenSchema.statics.hash(this.token);
  }

  if (this.refreshToken && this.isModified('refreshToken')) {
    this.refreshToken = TokenSchema.statics.hash(this.refreshToken);
  }

  next();
});

module.exports = mongoose.model('Token', TokenSchema);
