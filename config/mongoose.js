exports.default = {
  mongoose: function(api){
    return {
      connectionURL: process.env.MONGODB || process.env.MONGOLAB_URI || 'mongodb://localhost/test',
      modelPath: api.projectRoot + '/models'
    };
  }
};
