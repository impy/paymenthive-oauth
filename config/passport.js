exports.default = {
  passport: function(api){
    return {
      expires_in: 3600, // 1 hour token expiration time
      strategiesPath: api.projectRoot + '/strategies',
      facebook: {
        clientID: process.env.FACEBOOK_ID || 'ID',
        clientSecret: process.env.FACEBOOK_SECRET || 'SECRET'
      },
      linkedin: {
        clientID: process.env.LINKEDIN_ID || 'ID',
        clientSecret: process.env.LINKEDIN_SECRET || 'SECRET'
      }
    };
  }
};
